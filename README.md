# Gitlab CI Sentry

This project provides a pipeline for configuring sentry. Once sentry is configured, a subsequent ci
job may retrieve the necessary dsn, or the application may be configured manually.

## Using This Pipeline

This template can be imported and overridden by individual projects without modifying the original
configuration. Documentation and examples on how to use this feature can be found here:
https://docs.gitlab.com/ee/ci/yaml/#include

In order to use this template, copy the following into the project's `.gitlab-ci.yml`:

```yaml
include:
  - remote: "https://gitlab.com/byuhbll/lib/gitlab-ci/sentry/raw/1.0.0/template.yml"

stages:
  - sentry
```
